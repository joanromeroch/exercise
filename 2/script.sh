## Install docker
apt-get install -y curl && curl -fsSL https://get.docker.com -o get-docker.sh
sh get-docker.sh && sudo usermod -aG docker $USER

## Install docker-compose (I prefer to use docker-compose and not docker-run)
curl -L "https://github.com/docker/compose/releases/download/1.27.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
docker-compose up -d

## Check elasticsearch health
echo "Wait ..."
sleep 50
until  curl localhost:9200/_cat/health
do
        sleep 30
done