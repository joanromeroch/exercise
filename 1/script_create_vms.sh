﻿#!/bin/bash

# Create a resource group.
az group create --name resource1 --location westeurope

# Create a virtual network
az network vnet create --resource-group resource1 --name Vnet1 --address-prefix 10.0.0.0/16 \
  --subnet-name Subnet1 --subnet-prefix 10.0.1.0/24


# VM1
az vm create \
  --resource-group resource1 \
  --name vm1 --image UbuntuLTS \
  --admin-username azureuser \
  --admin-password MfpAl*dakl*a9D \
  --vnet-name Vnet1 --subnet Subnet1 \
  --nsg SecurityGroup1 --no-wait \
  --generate-ssh-keys

# VM2
az vm create \
  --resource-group resource1 \
  --name vm2 --image UbuntuLTS \
  --admin-username azureuser \
  --admin-password MfpAl*da76kl!a9D \
  --vnet-name Vnet1 --subnet Subnet1 \
  --nsg SecurityGroup1 --no-wait \
  --generate-ssh-keys


# Update security group and allow connection vm1 and vm2
az network nsg rule update --resource-group resource1 --nsg-name SecurityGroup1 \
  --source-address-prefix 10.0.1.0/24 --source-port-range '*' --destination-address-prefix '*' \
  --destination-port-range 22 --access allow

  