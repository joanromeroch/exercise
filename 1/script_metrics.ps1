﻿### Check metrics POWERSHELL
$lista = ('vm1','vm2')
$metricsazure = ('Percentage CPU','Network in','Network Out','Disk Read Bytes','Disk Write Bytes')

foreach ($a in $lista)
{
    az vm monitor metrics tail --name $a -g resource1 --metric $metricsazure > result.txt
}